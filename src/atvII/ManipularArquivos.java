package atvII;


import java.io.*;

public class ManipularArquivos {
    private String arquivoContatos;

    public ManipularArquivos(String arquivoContatos) {
        this.arquivoContatos = arquivoContatos;
    }

    public String lerArquivo() {
        StringBuilder conteudo = new StringBuilder();
        try (BufferedReader leitor = new BufferedReader(new FileReader(arquivoContatos))) {
            String linha;
            while ((linha = leitor.readLine()) != null) {
                conteudo.append(linha).append("\n");
            }
        } catch (IOException e) {
            System.out.println("Falha na leitura do arquivo:");
        }
        return conteudo.toString();
    }

    public void escreverArquivo(String novoConteudo) {
        try (BufferedWriter leitor = new BufferedWriter(new FileWriter(arquivoContatos))) {
        	leitor.write(novoConteudo);
        } catch (IOException e) {
        	System.out.println("Falha na escrita do arquivo:");
        }
    }
}

