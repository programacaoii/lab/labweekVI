# Gerenciamento de Contatos com CRUD e HashMap

Este repositório contém um projeto Java que implementa um sistema simples de gerenciamento de contatos, permitindo ao usuário adicionar, consultar e remover contatos. Os contatos são armazenados em um arquivo chamado "contatos.txt". O programa utiliza um HashMap para armazenar os contatos, onde a chave é o nome do contato e o valor é um objeto do tipo Contato.

## Funcionalidades

- **Classe Contato**:
  - A classe Contato possui os atributos `nome` e `telefone` para armazenar informações de contato.

- **Classe GerenciadorContatos**:
  - A classe `GerenciadorContatos` é responsável por gerenciar as operações de CRUD (Create, Read, Update, Delete) dos contatos junto com a classe.
  - Possui métodos para carregar os contatos do arquivo, adicionar, consultar, remover e salvar os contatos no arquivo.
  - A classe encapsula a lógica de persistência, tratamento de exceções e manipulação dos contatos.

- **Classe ManipularArquivos**:
  - A classe `ManipularArquivos` é responsável por gerenciar as operações de leitura e escrita do arquivo passado pelo `GerenciadorContatos`.

- **Classe Main**:
  - A classe `Main` contém o método principal que interage com o usuário por meio da classe `Menu`
  - O menu permite adicionar, consultar,exibir todos os contatos e remover contatos, além de encerrar o programa.

# Execução


![Imagem 1](../img/output2.png) | ![Imagem 2](../img/output2.1.png) 
|:---:|:---:|
| Exibir todos os contatos | Adicionar novo contato e exibis lista |

| ![Imagem 1](../img/output2.2.png) | ![Imagem 2](../img/output2.3.png) |
|:---:|:---:|
| Pesquisar por nome | Remover contato e exibir lista |

# Diagrama de classe
<img src="../img/diagrama2.png" width="100%"><br>