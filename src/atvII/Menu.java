package atvII;

import java.util.Scanner;

public class Menu {
	public static void menu(String conteudoLido) {
		GerenciadorContatos gerenciador = new GerenciadorContatos(conteudoLido);
		Scanner scanner = new Scanner(System.in);

		int opcao;
		do {
			System.out.println("Menu:");
			System.out.println("1 - Adicionar contato");
			System.out.println("2 - Consultar contato");
			System.out.println("3 - Remover contato");
			System.out.println("4 - Exibir todos os Contatos");
			System.out.println("5 - Encerrar programa");
			System.out.print("Escolha uma opcao: ");
			opcao = scanner.nextInt();
			scanner.nextLine();

			switch (opcao) {
			case 1:
				System.out.print("Nome do contato: ");
				String nome = scanner.nextLine();
				System.out.print("Telefone do contato: ");
				String telefone = scanner.nextLine();
				gerenciador.adicionarContato(nome, telefone);
				break;
			case 2:
				System.out.print("Nome do contato a ser consultado: ");
				String nomeConsulta = scanner.nextLine();
				String telefoneEncontrado = gerenciador.consultarContato(nomeConsulta);
				System.out.println(telefoneEncontrado);
				break;
			case 3:
				System.out.print("Nome do contato a ser removido: ");
				String nomeRemocao = scanner.nextLine();
				gerenciador.removerContato(nomeRemocao);
				break;
			case 4:
				gerenciador.exibirTodosContatos();
				break;
			case 5:
				System.out.println("Encerrando o programa.");
				break;
			default:
				System.out.println("Opcao invalida.");
			}
			System.out.println();
		} while (opcao != 5);

		scanner.close();
	}
}
