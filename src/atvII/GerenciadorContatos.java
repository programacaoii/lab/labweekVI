package atvII;

import java.util.*;

public class GerenciadorContatos {
	private Map<String, Contato> contatos;
	private ManipularArquivos manipularArquivos;

	public GerenciadorContatos(String arquivoContatos) {
		manipularArquivos = new ManipularArquivos(arquivoContatos);
		contatos = new HashMap<>();
		carregarContatos();
	}

	private void carregarContatos() {
		String conteudo = manipularArquivos.lerArquivo();
		String[] linhas = conteudo.split("\n");
		for (String linha : linhas) {
			String[] partes = linha.split(",");
			if (partes.length == 2) {
				Contato contato = new Contato(partes[0], partes[1]);
				contatos.put(partes[0], contato);
			}
		}
	}

	public void adicionarContato(String nome, String telefone) {
		Contato contato = new Contato(nome, telefone);
		contatos.put(nome, contato);
		salvarContatos();
	}

	public void exibirTodosContatos() {
		for (Contato contato : contatos.values()) {
			System.out.println(contato);
		}
	}

	public String consultarContato(String nome) {
		Contato contato = contatos.get(nome);
		if (contato != null) {
			return contato.toString();
		} else {
			return "Contato não encontrado.";
		}
	}

	public void removerContato(String nome) {
		contatos.remove(nome);
		salvarContatos();
	}

	private void salvarContatos() {
		StringBuilder novoConteudo = new StringBuilder();
		for (Contato contato : contatos.values()) {
			novoConteudo.append(contato.getNome()).append(",").append(contato.getTelefone()).append("\n");
		}
		manipularArquivos.escreverArquivo(novoConteudo.toString());
	}
}
