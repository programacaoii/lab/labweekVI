package atvI;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ManipularArquivo {

	public static String lerArquivo(String caminhoArquivo) {
		StringBuilder conteudo = new StringBuilder();
		try {
			File arquivo = new File(caminhoArquivo);
			FileReader leitor = new FileReader(arquivo);
			int caractere;
			while ((caractere = leitor.read()) != -1) {
				conteudo.append((char) caractere);
			}

			leitor.close();
		} catch (IOException e) {
			System.out.println("Ocorreu um erro ao ler o arquivo: " + e.getMessage());
		}
		return conteudo.toString();
	}
}
