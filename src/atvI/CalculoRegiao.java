package atvI;

import java.util.HashMap;

public class CalculoRegiao {

	public static HashMap<String, Regiao> calcularMedias(String conteudoArquivo) {
        HashMap<String, Regiao> mapaRegioes = new HashMap<>();
        double somaExpectativaVida = 0.0;
        double mediaExpectativaVida=0.0;
        String[] linhas = conteudoArquivo.split("\\n");
        
        for (int i = 1; i < linhas.length; i++) {
            String[] partes = linhas[i].split(";");
            String nomeRegiao = partes[0];
            
            
            for (int j = 1; j < partes.length; j++) {
                somaExpectativaVida += Double.parseDouble(partes[j]);
                
            }

            mediaExpectativaVida = somaExpectativaVida / (partes.length - 1);

            Regiao regiao = new Regiao(nomeRegiao, mediaExpectativaVida);
            mapaRegioes.put(nomeRegiao, regiao);
        }
        
        return mapaRegioes;
    }
}

