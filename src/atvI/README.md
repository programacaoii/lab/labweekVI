# Processamento de Dados com HashMap

Este repositório contém um projeto Java que realiza o processamento de dados de um arquivo CSV contendo informações sobre a expectativa de vida no Brasil e suas regiões de 2000 a 2014. O programa lê o arquivo linha a linha, realiza o cálculo da média da expectativa de vida para cada região e armazena os resultados em um HashMap. No final, o programa exibe o nome de cada região ao lado de sua média de expectativa de vida.


# Processamento de Dados com HashMap

Este repositório contém um projeto Java que realiza o processamento de dados de um arquivo CSV contendo informações sobre a expectativa de vida no Brasil e suas regiões de 2000 a 2014. O programa lê o arquivo linha a linha, realiza o cálculo da média da expectativa de vida para cada região e armazena os resultados em um HashMap. No final, o programa exibe o nome de cada região ao lado de sua média de expectativa de vida.

## Funcionalidades

- Leitura do arquivo `pesquisa`: O programa lê o arquivo contendo os dados de expectativa de vida, separados por ponto-e-vírgula (;).
  
- Cálculo da média de expectativa de vida: Para cada região, o programa calcula a média aritmética das expectativas de vida ao longo dos anos de 2000 a 2014.

- Armazenamento em HashMap: As médias calculadas são armazenadas em um HashMap, onde a chave é o nome da região e o valor é a média de expectativa de vida.

- Exibição dos resultados: Após o cálculo e armazenamento das médias, o programa exibe na saída as regiões e suas respectivas médias de expectativa de vida.

# Execução
<img src="../img/output1.png" width="70%"><br>

# Diagrama de classe
<img src="../img/diagrama1.png" width="70%"><br>



