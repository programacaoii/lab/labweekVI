package atvI;

public class Regiao {
    private String nome;
    private double mediaVida;

    public Regiao(String nome, double mediaVida) {
        this.nome = nome;
        this.mediaVida = mediaVida;
    }

    public String getNome() {
        return nome;
    }

    public double getMediaExpectativaVida() {
        return mediaVida;
    }

    @Override
    public String toString() {
        return String.format("Regiao: %-10s |  Media de vida: %.2f", nome, mediaVida);
    }
}
