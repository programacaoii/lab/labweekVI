package atvI;

import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
       
        String caminhoArquivo = "src/pesquisa";
        ManipularArquivo arquivo = new ManipularArquivo();
        String conteudoLido = arquivo.lerArquivo(caminhoArquivo);
     
       
        HashMap<String, Regiao> mapaRegioes = CalculoRegiao.calcularMedias(conteudoLido);
  
  
        for (Regiao regiao : mapaRegioes.values()) {
        	
            System.out.println(regiao);
        }
    }
}
