# Laboratório - Semana VI: Programação

Bem-vindo ao laboratório da Semana VI de programaçãoII!.

## Atividades

Aqui estão os links para acessar os arquivos de cada atividade:

- <a href="https://gitlab.com/programacaoii/lab/labweekVI/-/tree/main/src/atvI?ref_type=heads" target="_blank">Atividade 1</a>
- <a href="https://gitlab.com/programacaoii/lab/labweekVI/-/tree/main/src/atvII?ref_type=heads" target="_blank">Atividade 2</a>

Clique nos links acima para abrir cada arquivo em uma nova aba do seu navegador.



